angular.module( 'ngBoilerplate.studyTour', [
    'ui.router',
    'placeholders',
    'ui.bootstrap'
])

    .config(function config( $stateProvider ) {
        $stateProvider.state( 'studyTour', {
            url: '/studyTour',
            views: {
                "main": {
                    controller: 'StudyTourCtrl',
                    templateUrl: 'imageGallery/study-tour/studyTour.tpl.html'
                }
            },
            data:{ pageTitle: 'Study Tour' }
        });
    })

    .controller( 'StudyTourCtrl', function StudyTourCtrl( $scope,$http,$rootScope, Lightbox ) {
        //$scope.linkurl = [  {link: "assets/images/imageGallery/study-tour/1.jpg"},
        //                    {link: "assets/images/imageGallery/study-tour/2.jpg"},
        //                    {link: "assets/images/imageGallery/study-tour/3.jpg"},
        //                    {link: "assets/images/imageGallery/study-tour/4.jpg"},
        //                    {link: "assets/images/imageGallery/study-tour/5.jpg"},
        //                    {link: "assets/images/imageGallery/study-tour/6.jpg"},
        //                    {link: "assets/images/imageGallery/study-tour/7.jpg"},
        //                    {link: "assets/images/imageGallery/study-tour/8.jpg"},
        //                    {link: "assets/images/imageGallery/study-tour/9.jpg"},
        //                    {link: "assets/images/imageGallery/study-tour/10.jpg"},
        //                    {link: "assets/images/imageGallery/study-tour/11.jpg"},
        //                    {link: "assets/images/imageGallery/study-tour/12.jpg"},
        //                    {link: "assets/images/imageGallery/study-tour/13.jpg"},
        //                    {link: "assets/images/imageGallery/study-tour/14.jpg"}
        //                ];

        $scope.images = [
            {
                'url': 'assets/images/imageGallery/study-tour/1.jpg',
                //'caption': 'Optional caption',
                'thumbUrl': 'assets/images/imageGallery/study-tour/1.jpg'
            },
            {
                'url': 'assets/images/imageGallery/study-tour/2.jpg',
                'thumbUrl': 'assets/images/imageGallery/study-tour/2.jpg'
            },
            {
                'url': 'assets/images/imageGallery/study-tour/3.jpg',
                'thumbUrl': 'assets/images/imageGallery/study-tour/3.jpg'
            },
            {
                'url': 'assets/images/imageGallery/study-tour/4.jpg',
                'thumbUrl': 'assets/images/imageGallery/study-tour/4.jpg'
            },
            {
                'url': 'assets/images/imageGallery/study-tour/5.jpg',
                'thumbUrl': 'assets/images/imageGallery/study-tour/5.jpg'
            },
            {
                'url': 'assets/images/imageGallery/study-tour/6.jpg',
                'thumbUrl': 'assets/images/imageGallery/study-tour/6.jpg'
            },
            {
                'url': 'assets/images/imageGallery/study-tour/7.jpg',
                'thumbUrl': 'assets/images/imageGallery/study-tour/7.jpg'
            },
            {
                'url': 'assets/images/imageGallery/study-tour/8.jpg',
                'thumbUrl': 'assets/images/imageGallery/study-tour/8.jpg'
            },
            {
                'url': 'assets/images/imageGallery/study-tour/9.jpg',
                'thumbUrl': 'assets/images/imageGallery/study-tour/9.jpg'
            },
            {
                'url': 'assets/images/imageGallery/study-tour/10.jpg',
                'thumbUrl': 'assets/images/imageGallery/study-tour/10.jpg'
            },
            {
                'url': 'assets/images/imageGallery/study-tour/11.jpg',
                'thumbUrl': 'assets/images/imageGallery/study-tour/11.jpg'
            },
            {
                'url': 'assets/images/imageGallery/study-tour/12.jpg',
                'thumbUrl': 'assets/images/imageGallery/study-tour/12.jpg'
            },
            {
                'url': 'assets/images/imageGallery/study-tour/13.jpg',
                'thumbUrl': 'assets/images/imageGallery/study-tour/13.jpg'
            },
            {
                'url': 'assets/images/imageGallery/study-tour/14.jpg',
                'thumbUrl': 'assets/images/imageGallery/study-tour/14.jpg'
            }
        ];

        $scope.openLightboxModal = function (index) {
            Lightbox.openModal($scope.images, index);
        };
    });
