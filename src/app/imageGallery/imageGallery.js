angular.module( 'ngBoilerplate.imageGallery', [
    'ui.router',
    'placeholders',
    'ui.bootstrap'
])

    .config(function config( $stateProvider ) {
        $stateProvider.state( 'imageGallery', {
            url: '/imageGallery',
            views: {
                "main": {
                    controller: 'ImageGalleryCtrl',
                    templateUrl: 'imageGallery/imageGallery.tpl.html'
                }
            },
            data:{ pageTitle: 'Image Gallery' }
        });
    })

    .controller( 'ImageGalleryCtrl', function ImageGalleryCtrl( $scope,$http,$rootScope ) {

    });
