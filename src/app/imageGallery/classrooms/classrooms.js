angular.module( 'ngBoilerplate.classrooms', [
    'ui.router',
    'placeholders',
    'ui.bootstrap'
])

    .config(function config( $stateProvider ) {
        $stateProvider.state( 'classrooms', {
            url: '/classrooms',
            views: {
                "main": {
                    controller: 'ClassroomsCtrl',
                    templateUrl: 'imageGallery/classrooms/classrooms.tpl.html'
                }
            },
            data:{ pageTitle: 'Classrooms' }
        });
    })

    .controller( 'ClassroomsCtrl', function ClassroomsCtrl( $scope, $http, $rootScope, Lightbox ) {
        //$scope.linkurl = [  {link: "assets/images/imageGallery/classrooms/1.jpg"},
        //                {link: "assets/images/imageGallery/classrooms/2.jpg"},
        //                {link: "assets/images/imageGallery/classrooms/3.jpg"},
        //                {link: "assets/images/imageGallery/classrooms/4.jpg"},
        //                {link: "assets/images/imageGallery/classrooms/5.jpg"},
        //                {link: "assets/images/imageGallery/classrooms/6.jpg"},
        //                {link: "assets/images/imageGallery/classrooms/7.jpg"},
        //                {link: "assets/images/imageGallery/classrooms/8.jpg"},
        //                {link: "assets/images/imageGallery/classrooms/9.jpg"},
        //                {link: "assets/images/imageGallery/classrooms/10.jpg"},
        //                {link: "assets/images/imageGallery/classrooms/11.jpg"},
        //                {link: "assets/images/imageGallery/classrooms/12.jpg"},
        //                {link: "assets/images/imageGallery/classrooms/13.jpg"},
        //                {link: "assets/images/imageGallery/classrooms/14.jpg"},
        //                {link: "assets/images/imageGallery/classrooms/15.jpg"},
        //                {link: "assets/images/imageGallery/classrooms/16.jpg"},
        //                {link: "assets/images/imageGallery/classrooms/17.jpg"},
        //                {link: "assets/images/imageGallery/classrooms/18.jpg"},
        //                {link: "assets/images/imageGallery/classrooms/19.jpg"},
        //                {link: "assets/images/imageGallery/classrooms/20.jpg"}
        //                ];

        $scope.images = [
            {
                'url': 'assets/images/imageGallery/classrooms/1.jpg',
                //'caption': 'Optional caption',
                'thumbUrl': 'assets/images/imageGallery/classrooms/1.jpg'
            },
            {
                'url': 'assets/images/imageGallery/classrooms/2.jpg',
                'thumbUrl': 'assets/images/imageGallery/classrooms/2.jpg'
            },
            {
                'url': 'assets/images/imageGallery/classrooms/3.jpg',
                'thumbUrl': 'assets/images/imageGallery/classrooms/3.jpg'
            },
            {
                'url': 'assets/images/imageGallery/classrooms/4.jpg',
                'thumbUrl': 'assets/images/imageGallery/classrooms/4.jpg'
            },
            {
                'url': 'assets/images/imageGallery/classrooms/5.jpg',
                'thumbUrl': 'assets/images/imageGallery/classrooms/5.jpg'
            },
            {
                'url': 'assets/images/imageGallery/classrooms/6.jpg',
                'thumbUrl': 'assets/images/imageGallery/classrooms/6.jpg'
            },
            {
                'url': 'assets/images/imageGallery/classrooms/7.jpg',
                'thumbUrl': 'assets/images/imageGallery/classrooms/7.jpg'
            },
            {
                'url': 'assets/images/imageGallery/classrooms/8.jpg',
                'thumbUrl': 'assets/images/imageGallery/classrooms/8.jpg'
            },
            {
                'url': 'assets/images/imageGallery/classrooms/9.jpg',
                'thumbUrl': 'assets/images/imageGallery/classrooms/9.jpg'
            },
            {
                'url': 'assets/images/imageGallery/classrooms/10.jpg',
                'thumbUrl': 'assets/images/imageGallery/classrooms/10.jpg'
            },
            {
                'url': 'assets/images/imageGallery/classrooms/11.jpg',
                'thumbUrl': 'assets/images/imageGallery/classrooms/11.jpg'
            },
            {
                'url': 'assets/images/imageGallery/classrooms/12.jpg',
                'thumbUrl': 'assets/images/imageGallery/classrooms/12.jpg'
            },
            {
                'url': 'assets/images/imageGallery/classrooms/13.jpg',
                'thumbUrl': 'assets/images/imageGallery/classrooms/13.jpg'
            },
            {
                'url': 'assets/images/imageGallery/classrooms/14.jpg',
                'thumbUrl': 'assets/images/imageGallery/classrooms/14.jpg'
            },
            {
                'url': 'assets/images/imageGallery/classrooms/15.jpg',
                'thumbUrl': 'assets/images/imageGallery/classrooms/15.jpg'
            },{
                'url': 'assets/images/imageGallery/classrooms/16.jpg',
                'thumbUrl': 'assets/images/imageGallery/classrooms/16.jpg'
            },
            {
                'url': 'assets/images/imageGallery/classrooms/17.jpg',
                'thumbUrl': 'assets/images/imageGallery/classrooms/17.jpg'
            },
            {
                'url': 'assets/images/imageGallery/classrooms/18.jpg',
                'thumbUrl': 'assets/images/imageGallery/classrooms/18.jpg'
            },
            {
                'url': 'assets/images/imageGallery/classrooms/19.jpg',
                'thumbUrl': 'assets/images/imageGallery/classrooms/19.jpg'
            },
            {
                'url': 'assets/images/imageGallery/classrooms/20.jpg',
                'thumbUrl': 'assets/images/imageGallery/classrooms/20.jpg'
            }
        ];

        $scope.openLightboxModal = function (index) {
            Lightbox.openModal($scope.images, index);
        };
    });
