angular.module( 'ngBoilerplate.awardGivingCeremony', [
    'ui.router',
    'placeholders',
    'ui.bootstrap'
])

    .config(function config( $stateProvider ) {
        $stateProvider
        .state( 'awardGivingCeremony', {
            url: '/awardGivingCeremony',
            views: {
                "main": {
                    controller: 'AwardGivingCeremonyCtrl',
                    templateUrl: 'imageGallery/award-giving-ceremony/awardGivingCeremony.tpl.html'
                }
            },
            data:{ pageTitle: 'Award Giving Ceremony' }
        });
    })

    .controller( 'AwardGivingCeremonyCtrl', function AwardGivingCeremonyCtrl( $scope, $http, $rootScope, Lightbox ) {
    //$scope.linkurl = [  {link: "assets/images/imageGallery/award-giving-ceremony/1.jpg"},
    //                    {link: "assets/images/imageGallery/award-giving-ceremony/2.jpg"},
    //                    {link: "assets/images/imageGallery/award-giving-ceremony/3.jpg"},
    //                    {link: "assets/images/imageGallery/award-giving-ceremony/4.jpg"},
    //                    {link: "assets/images/imageGallery/award-giving-ceremony/5.jpg"},
    //                    {link: "assets/images/imageGallery/award-giving-ceremony/6.jpg"},
    //                    {link: "assets/images/imageGallery/award-giving-ceremony/7.jpg"},
    //                    {link: "assets/images/imageGallery/award-giving-ceremony/8.jpg"},
    //                    {link: "assets/images/imageGallery/award-giving-ceremony/9.jpg"},
    //                    {link: "assets/images/imageGallery/award-giving-ceremony/10.jpg"},
    //                    {link: "assets/images/imageGallery/award-giving-ceremony/11.jpg"},
    //                    {link: "assets/images/imageGallery/award-giving-ceremony/12.jpg"},
    //                    {link: "assets/images/imageGallery/award-giving-ceremony/13.jpg"},
    //                    {link: "assets/images/imageGallery/award-giving-ceremony/14.jpg"},
    //                    {link: "assets/images/imageGallery/award-giving-ceremony/15.jpg"}
    //                    ];

        $scope.images = [
            {
                'url': 'assets/images/imageGallery/award-giving-ceremony/1.jpg',
                //'caption': 'Optional caption',
                'thumbUrl': 'assets/images/imageGallery/award-giving-ceremony/1.jpg'
            },
            {
                'url': 'assets/images/imageGallery/award-giving-ceremony/2.jpg',
                'thumbUrl': 'assets/images/imageGallery/award-giving-ceremony/2.jpg'
            },
            {
                'url': 'assets/images/imageGallery/award-giving-ceremony/3.jpg',
                'thumbUrl': 'assets/images/imageGallery/award-giving-ceremony/3.jpg'
            },
            {
                'url': 'assets/images/imageGallery/award-giving-ceremony/4.jpg',
                'thumbUrl': 'assets/images/imageGallery/award-giving-ceremony/4.jpg'
            },
            {
                'url': 'assets/images/imageGallery/award-giving-ceremony/5.jpg',
                'thumbUrl': 'assets/images/imageGallery/award-giving-ceremony/5.jpg'
            },
            {
                'url': 'assets/images/imageGallery/award-giving-ceremony/6.jpg',
                'thumbUrl': 'assets/images/imageGallery/award-giving-ceremony/6.jpg'
            },
            {
                'url': 'assets/images/imageGallery/award-giving-ceremony/7.jpg',
                'thumbUrl': 'assets/images/imageGallery/award-giving-ceremony/7.jpg'
            },
            {
                'url': 'assets/images/imageGallery/award-giving-ceremony/8.jpg',
                'thumbUrl': 'assets/images/imageGallery/award-giving-ceremony/8.jpg'
            },
            {
                'url': 'assets/images/imageGallery/award-giving-ceremony/9.jpg',
                'thumbUrl': 'assets/images/imageGallery/award-giving-ceremony/9.jpg'
            },
            {
                'url': 'assets/images/imageGallery/award-giving-ceremony/10.jpg',
                'thumbUrl': 'assets/images/imageGallery/award-giving-ceremony/10.jpg'
            },
            {
                'url': 'assets/images/imageGallery/award-giving-ceremony/11.jpg',
                'thumbUrl': 'assets/images/imageGallery/award-giving-ceremony/11.jpg'
            },
            {
                'url': 'assets/images/imageGallery/award-giving-ceremony/12.jpg',
                'thumbUrl': 'assets/images/imageGallery/award-giving-ceremony/12.jpg'
            },
            {
                'url': 'assets/images/imageGallery/award-giving-ceremony/13.jpg',
                'thumbUrl': 'assets/images/imageGallery/award-giving-ceremony/13.jpg'
            },
            {
                'url': 'assets/images/imageGallery/award-giving-ceremony/14.jpg',
                'thumbUrl': 'assets/images/imageGallery/award-giving-ceremony/14.jpg'
            },
            {
                'url': 'assets/images/imageGallery/award-giving-ceremony/15.jpg',
                'thumbUrl': 'assets/images/imageGallery/award-giving-ceremony/15.jpg'
            }
        ];

        $scope.openLightboxModal = function (index) {
            Lightbox.openModal($scope.images, index);
        };
    });
