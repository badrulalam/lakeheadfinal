/**
 * Each section of the site has its own module. It probably also has
 * submodules, though this boilerplate is too simple to demonstrate it. Within
 * `src/app/home`, however, could exist several additional folders representing
 * additional modules that would then be listed as dependencies of this one.
 * For example, a `note` section could have the submodules `note.create`,
 * `note.delete`, `note.edit`, etc.
 *
 * Regardless, so long as dependencies are managed correctly, the build process
 * will automatically take take of the rest.
 *
 * The dependencies block here is also where component dependencies should be
 * specified, as shown below.
 */
var nghome = angular.module( 'ngBoilerplate.home', [
  'ui.router',
  'plusOne'
])

/**
 * Each section or module of the site can also have its own routes. AngularJS
 * will handle ensuring they are all available at run-time, but splitting it
 * this way makes each module more "self-contained".
 */
    .config(function config( $stateProvider ) {
      $stateProvider
          .state( 'home', {
            url: '/',
            views: {
              "main": {
                controller: 'HomeCtrl',
                templateUrl: 'home/home.tpl.html'
              }
            },
            data:{ pageTitle: 'Home' }
          });
    });

/**
 * And of course we define a controller for our route.
 */
nghome.controller( 'HomeCtrl', function HomeController( $scope ) {
  //$(window).scrollTop(0);
});

nghome.controller('CarouselDemoCtrl', function ($scope) {
  var height = window.innerHeight - 142 +"px";
  $scope.carouselHeight = {"height": height};
  $scope.carouselStyle = {"margin": "auto"};
  $(window).scrollTop(0);
  $scope.myInterval = 5000;
  var slides = $scope.slides = [
    {image:'assets/images/slide-1.jpg',
      title: "Your Child needs to explore",
      subtitle: "Opportunities for physical, cognitive and emotional development A secure and safe environment so that the child will not be afraid of new experiences."
    },
    {image:'assets/images/slide-2.png',
      title: "We Build champions",
      subtitle: "Varied experiences for learning and completing developmental tasks.Freedom, opportunity and encouragement towards developing responsibility, self control and independence while maintaning respect for others."
    },
    {image:'assets/images/slide-3.png',
      title: "We are Equiped",
      subtitle: "We are well equiped."
    }
  ];
  //$scope.addSlide = function() {
  //  var newWidth = 600 + slides.length + 1;
  //  slides.push({
  //    image: 'http://placekitten.com/' + newWidth + '/300',
  //    text: ['More','Extra','Lots of','Surplus'][slides.length % 4] + ' ' +
  //    ['Cats', 'Kittys', 'Felines', 'Cutes'][slides.length % 4]
  //  });
  //};

});

nghome.controller('UpdateLeftCtrl', function ($scope, $http, $rootScope) {
  //------Dynamic Start----------------------
  $scope.blink = $rootScope.apipath;

  $http.get($rootScope.apipath+"/event")
      .success(function (data) {
        //On successful API CALL check whether empty data is returned or not

        //console.log('Event data', data);
        $scope.allevent = data;
      })
      .error(function (data) {
        //Log error Data
        // $log.info(data);
        //console.log('error');
      });

  //------Dynamic End------------------------

});

nghome.controller('LatestRightCtrl', function ($scope, $http,$rootScope) {

  //------Dynamic Start----------------------

  $http.get($rootScope.apipath+"/news")
      .success(function (data) {
        //On successful API CALL check whether empty data is returned or not

        //console.log('News data', data);
        $scope.allnews = data;
      })
      .error(function (data) {
        //Log error Data
        // $log.info(data);
        //console.log('error');
      });

  //------Dynamic End------------------------
});

nghome.controller('CarouselTwoCtrl', function ($scope,$http) {
  $scope.IntervalTwo = 3000;
  var slidestwo = $scope.slidestwo = [
    {image:'assets/images/slider-1.jpg'},
    {image:'assets/images/slider-2.jpg'},
    {image:'assets/images/slider-3.jpg'},
    {image:'assets/images/slider-4.jpg'}
  ];

  function gallary(){
    $http.get('http://lakehead.zeteq.com:1340/imggallery').success(function(response){
      //console.log('gallary',response);
      response.forEach(function(val){
        //console.log('6images',val.galimages[0].image);
        //console.log('6images',val.name);
      });
    });
  }
  gallary();

});

nghome.controller('ContactCtrl', function ContactusCtrl($scope, $http, $rootScope, $window) {
  $scope.sendEmail = function(email){
    //console.log('Sendmail',email);
    //email.subject = "Lakehead Grammar";
    $http.post($rootScope.apipath + '/mail/sendmail',email).success(function(){
      //console.log("Successfully sent");
      $scope.email = {};
      $window.alert('Successfully sent to Your Email');
    });
  };
});
