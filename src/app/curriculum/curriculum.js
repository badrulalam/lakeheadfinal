angular.module( 'ngBoilerplate.curriculum', [
  'ui.router',
  'placeholders',
  'ui.bootstrap'
])

.config(function config( $stateProvider ) {
  $stateProvider.state( 'curriculum', {
    url: '/curriculum',
    views: {
      "main": {
        controller: 'CurriculumCtrl',
        templateUrl: 'curriculum/curriculum.tpl.html'
      }
    },
    data:{ pageTitle: 'Curriculum' }
  })
      .state('curriculum.slugpage', {
          url: '/:slug',
          views: {
              "about": {
                  controller: 'CurSlugpageCtrl',
                  templateUrl: 'curriculum/slugpage.tpl.html'
              }
          }
      })

      //.state( 'curriculum.elementary', {
      //  url: '/elementary',
      //  views: {
      //    "curriculum": {
      //      controller: 'ElementaryCtrl',
      //      templateUrl: 'curriculum/elementary.tpl.html'
      //    }
      //  },
      //  data:{ pageTitle: 'Elementary' }
      //})
      //.state( 'curriculum.primary', {
      //  url: '/primary',
      //  views: {
      //    "curriculum": {
      //      controller: 'PrimaryCtrl',
      //      templateUrl: 'curriculum/primary.tpl.html'
      //    }
      //  },
      //  data:{ pageTitle: 'primary' }
      //})
      //.state( 'curriculum.middle', {
      //  url: '/middle',
      //  views: {
      //    "curriculum": {
      //      controller: 'MiddleCtrl',
      //      templateUrl: 'curriculum/middle.tpl.html'
      //    }
      //  },
      //  data:{ pageTitle: 'middle' }
      //})
      //.state( 'curriculum.final', {
      //  url: '/final',
      //  views: {
      //    "curriculum": {
      //      controller: 'FinalCtrl',
      //      templateUrl: 'curriculum/final.tpl.html'
      //    }
      //  },
      //  data:{ pageTitle: 'final' }
      //})
  ;
})

//.controller( 'CurriculumCtrl', function AboutCtrl( $scope ) {
//  // This is simple a demo for UI Boostrap.
//  $scope.dropdownDemoItems = [
//    "The first choice!",
//    "And another choice for you.",
//    "but wait! A third!"
//  ];
//})

    .controller('CurriculumCtrl', function CurriculumCtrl($scope,$http,$rootScope) {
      // This is simple a demo for UI Boostrap.
      $scope.dropdownDemoItems = [
        "The first choice!",
        "And another choice for you.",
        "but wait! A third!"
      ];

      //------Dynamic Start----------------------

      $scope.pageslug='curriculum';
      // $scope.content='asdds';

      $http.get($rootScope.apipath+"/page?slug="+$scope.pageslug)
          .success(function(data){
            //On successful API CALL check whether empty data is returned or not

            //console.log('slug data',data);
            $scope.content=data;
          })
          .error(function(data){
            //Log error Data
            // $log.info(data);
            //console.log('error');
          });

      //------Dynamic End------------------------

    })

    .controller('CurSlugpageCtrl', function CurSlugpageCtrl($scope,$http,$stateParams,$rootScope) {
        // This is simple a demo for UI Boostrap.

        //console.log('stateparm',$stateParams);

        //------Dynamic Start----------------------

        $scope.pageslug=$stateParams.slug;
         $scope.content={};

        $http.get($rootScope.apipath+"/page?slug="+$scope.pageslug)
            .success(function(data){
                //On successful API CALL check whether empty data is returned or not

                //console.log('slug data',data);
                $scope.content=data;
            })
            .error(function(data){
                //Log error Data
                // $log.info(data);
                //console.log('error');
            });

        //------Dynamic End------------------------

    });
