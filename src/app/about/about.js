angular.module('ngBoilerplate.about', [
    'ui.router',
    'placeholders',
    'ui.bootstrap'
])

    .config(function config($stateProvider) {
        $stateProvider
        .state('about', {
            url: '/about',
            views: {
                "main": {
                    controller: 'AboutCtrl',
                    templateUrl: 'about/about.tpl.html'
                }
            },
            data: {pageTitle: 'About Us'}
        })
        .state('about.slugpage', {
            url: '/:slug',
            views: {
                "about": {
                    controller: 'SlugpageCtrl',
                    templateUrl: 'about/slugpage.tpl.html'
                }
            }
        });
    })

    .controller('AboutCtrl', function AboutCtrl($scope, $http, $rootScope) {


        //------Dynamic Start----------------------

        $scope.pageslug = 'about-us';
        // $scope.content='asdds';

        $http.get($rootScope.apipath+"/page?slug=" + $scope.pageslug)
            .success(function (data) {
                //On successful API CALL check whether empty data is returned or not

                //console.log('slug data', data);
                $scope.laloader = true;
                $scope.content = data;
            })
            .error(function (data) {
                //Log error Data
                // $log.info(data);
                //console.log('error');
            });


        //------Dynamic End------------------------


    })


    .controller('SlugpageCtrl', function SlugpageCtrl($scope, $http, $stateParams, $rootScope) {
        // This is simple a demo for UI Boostrap.

        //console.log('stateparm', $stateParams);

        //------Dynamic Start----------------------

        $scope.pageslug = $stateParams.slug;
        $scope.content={};

        $http.get($rootScope.apipath+"/page?slug=" + $scope.pageslug)
            .success(function (data) {
                //On successful API CALL check whether empty data is returned or not

                //console.log('slug data', data);
                $scope.content = data;
            })
            .error(function (data) {
                //Log error Data
                // $log.info(data);
                //console.log('error');
            });


        //------Dynamic End------------------------


    })


;
