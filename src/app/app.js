angular.module( 'ngBoilerplate', [
  'templates-app',
  'templates-common',
  'ngBoilerplate.home',
  'ngBoilerplate.about',
  'ngBoilerplate.curriculum',
  'ngBoilerplate.admission',
  'ngBoilerplate.career',
  'ngBoilerplate.contactus',
  'ngBoilerplate.pupils',
  'ngBoilerplate.parents',
  'ngBoilerplate.imageGallery',
  'ngBoilerplate.awardGivingCeremony',
  'ngBoilerplate.classrooms',
  'ngBoilerplate.studyTour',
  'ui.router',
  'bootstrapLightbox',
  'ngTouch',
  'angular-loading-bar',
  'ngAnimate',
  'ngSanitize'
])

.config( function myAppConfig ( $stateProvider, $urlRouterProvider ) {
  $urlRouterProvider.otherwise( '/' );
})

.run( function run ($rootScope) {
      //$rootScope.apipath = 'http://localhost:1337';
      //$rootScope.apipath = 'http://zeteq.com:1340';
      $rootScope.apipath = 'http://lakeheadbackend.zeteq.com';
})

.controller( 'AppCtrl', function AppCtrl ( $scope, $location ) {
  $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
    if ( angular.isDefined( toState.data.pageTitle ) ) {
      $scope.pageTitle = toState.data.pageTitle + ' | Lakehead' ;
    }
  });
});
