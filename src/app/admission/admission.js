angular.module('ngBoilerplate.admission', [
    'ui.router',
    'placeholders',
    'ui.bootstrap'
])
    .config(function config($stateProvider) {
        $stateProvider
            .state('admission', {
                url: '/admission',
                views: {
                    "main": {
                        controller: 'AdmissionCtrl',
                        templateUrl: 'admission/admission.tpl.html'
                    }
                },
                data: {pageTitle: 'Addmission'}
            })
            .state('admission.slugpage', {
                url: '/:slug',
                views: {
                    "addmission": {
                        controller: 'AddSlugpageCtrl',
                        templateUrl: 'admission/slugpage.tpl.html'
                    }
                }
            });
    })
    .controller('AdmissionCtrl', function AdmissionCtrl($scope, $http, $rootScope) {
        // This is simple a demo for UI Boostrap.
        $scope.dropdownDemoItems = [
            "The first choice!",
            "And another choice for you.",
            "but wait! A third!"
        ];

        //------Dynamic Start----------------------

        $scope.pageslug = 'admission';
        // $scope.content='asdds';

        $http.get($rootScope.apipath + "/page?slug=" + $scope.pageslug)
            .success(function (data) {
                //On successful API CALL check whether empty data is returned or not

                //console.log('slug data', data);
                $scope.content = data;
            })
            .error(function (data) {
                //Log error Data
                // $log.info(data);
                //console.log('error');
            });

        //------Dynamic End------------------------
    })

    .controller('AddSlugpageCtrl', function AddSlugpageCtrl($scope, $http, $stateParams, $rootScope) {
        // This is simple a demo for UI Boostrap.
        //console.log('stateparm', $stateParams);

        //------Dynamic Start----------------------

        $scope.pageslug = $stateParams.slug;
        $scope.content = {};

        $http.get($rootScope.apipath + "/page?slug=" + $scope.pageslug)
            .success(function (data) {
                //On successful API CALL check whether empty data is returned or not
                //console.log('slug data', data);
                $scope.content = data;
            })
            .error(function (data) {
                //Log error Data
                // $log.info(data);
                //console.log('error');
            });

        //------Dynamic End------------------------

    });
