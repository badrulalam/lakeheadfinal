angular.module('ngBoilerplate.contactus', [
    'ui.router',
    'placeholders',
    'ui.bootstrap'
])

    .config(function config($stateProvider) {
        $stateProvider.state('contactus', {
            url: '/contactus',
            views: {
                "main": {
                    controller: 'ContactusCtrl',
                    templateUrl: 'contactus/contactus.tpl.html'
                }
            },
            data: {pageTitle: 'Contactus'}
        });
    })

    .controller('ContactusCtrl', function ContactusCtrl($scope, $http, $rootScope, $window) {
        $scope.sendMail = function (email) {
            //console.log('Sendmail',email);
            //$http.post('http://localhost:1337/mail/sendmail',email)
            $http.post($rootScope.apipath + '/mail/sendmail', email).success(function () {
                //console.log("Successfully sent");
                $scope.email = {};
                $window.alert('Successfully sent to Your Email');
            });
        };

        //------Dynamic Start----------------------

        $scope.pageslug = 'about-us';
        // $scope.content='asdds';

        //$http.get($rootScope.apipath + "/page/contmail")
        //    .success(function (data) {
        //        //On successful API CALL check whether empty data is returned or not
        //
        //        console.log('mail Data', data);
        //        $scope.laloader = true;
        //        $scope.content = data;
        //    })
        //    .error(function (data) {
        //        //Log error Data
        //        // $log.info(data);
        //        console.log('error');
        //    });


        //------Dynamic End------------------------

    });
