angular.module('ngBoilerplate.upcomingevents', [
    'ui.router',
    'placeholders',
    'ui.bootstrap'
])

    .config(function config($stateProvider) {
        $stateProvider
            .state('upcomingevents', {
                url: '/upcomingevents',
                views: {
                    "main": {
                        controller: 'UpcomingeventsCtrl',
                        templateUrl: 'upcomingevents.tpl.html'
                    }
                },
                data: {pageTitle: 'upcomingevents'}
            });
    })

    .controller('UpcomingeventsCtrl', function UpcomingeventsCtrl($scope, $http, $rootScope) {
        // This is simple a demo for UI Boostrap.
        $scope.dropdownDemoItems = [
            "The first choice!",
            "And another choice for you.",
            "but wait! A third!"
        ];
    });
