angular.module('ngBoilerplate.classrooms', [
    'ui.router',
    'placeholders',
    'ui.bootstrap'
])

    .config(function config($stateProvider) {
        $stateProvider
            .state('classrooms', {
                url: '/classrooms',
                views: {
                    "main": {
                        controller: 'ClassroomsCtrl',
                        templateUrl: 'classrooms.tpl.html'
                    }
                },
                data: {pageTitle: 'Classrooms'}
            });
    })

    .controller('ClassroomsCtrl', function ClassroomsCtrl($scope, $http, $rootScope) {
        // This is simple a demo for UI Boostrap.
        $scope.dropdownDemoItems = [
            "The first choice!",
            "And another choice for you.",
            "but wait! A third!"
        ];
    });
