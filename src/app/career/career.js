angular.module('ngBoilerplate.career', [
    'ui.router',
    'placeholders',
    'ui.bootstrap'
])

    .config(function config($stateProvider) {
        $stateProvider
            .state('career', {
                url: '/career',
                views: {
                    "main": {
                        controller: 'CareerCtrl',
                        templateUrl: 'career/career.tpl.html'
                    }
                },
                data: {pageTitle: 'career'}
            })
            .state('career.recruitment', {
                url: '/recruitment',
                views: {
                    "career": {
                        controller: 'RecruitmentCtrl',
                        templateUrl: 'career/recruitment.tpl.html'
                    }
                },
                data: {pageTitle: 'career'}
            });
    })

    .controller('CareerCtrl', function CareerCtrl($scope, $http, $rootScope) {
        // This is simple a demo for UI Boostrap.
        $scope.dropdownDemoItems = [
            "The first choice!",
            "And another choice for you.",
            "but wait! A third!"
        ];

        //------Dynamic Start----------------------

        $scope.pageslug = 'career';
        $scope.content = {};

        $http.get($rootScope.apipath + "/page?slug=" + $scope.pageslug)
            .success(function (data) {
                //On successful API CALL check whether empty data is returned or not

                //console.log('slug data', data);
                $scope.content = data;
            })
            .error(function (data) {
                //Log error Data
                // $log.info(data);
                //console.log('error');
            });

        //------Dynamic End------------------------

    })

    //.controller('RecruitmentCtrl', function RecruitmentCtrl($scope, $http, $rootScope) {
    //    // This is simple a demo for UI Boostrap.
    //    $scope.dropdownDemoItems = [
    //        "The first choice!",
    //        "And another choice for you.",
    //        "but wait! A third!"
    //    ];
    //});

    .controller('RecruitmentCtrl', function RecruitmentCtrl($scope, $http, $rootScope, $window) {
        $scope.sendResume = function (email) {
            //console.log('Sendmail',email);
            //email.uploadPdf = "Lakehead Grammar";
            $http.post($rootScope.apipath + '/mail/sendmail', email).success(function () {
                //console.log("Successfully sent");
                $scope.email = {};
                $window.alert('Successfully sent to Your Email');
            });
        };
    });
