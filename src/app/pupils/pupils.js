angular.module( 'ngBoilerplate.pupils', [
  'ui.router',
  'placeholders',
  'ui.bootstrap'
])

.config(function config( $stateProvider ) {
  $stateProvider
      .state( 'pupils', {
    url: '/pupils',
    views: {
      "main": {
        controller: 'PupilsCtrl',
        templateUrl: 'pupils/pupils.tpl.html'
      }
    },
    data:{ pageTitle: 'pupils' }
  })
      .state('pupils.slugpage', {
          url: '/:slug',
          views: {
              "pupils": {
                  controller: 'PuSlugpageCtrl',
                  templateUrl: 'pupils/slugpage.tpl.html'
              }
          }
      });
})

    .controller('PupilsCtrl', function PupilsCtrl($scope, $http, $rootScope) {
      // This is simple a demo for UI Boostrap.
      $scope.dropdownDemoItems = [
        "The first choice!",
        "And another choice for you.",
        "but wait! A third!"
      ];

      //------Dynamic Start----------------------

      $scope.pageslug = 'pupils';
      // $scope.content='asdds';

      $http.get($rootScope.apipath+"/page?slug=" + $scope.pageslug)
          .success(function (data) {
            //On successful API CALL check whether empty data is returned or not

            //console.log('slug data', data);
            $scope.content = data;
          })
          .error(function (data) {
            //Log error Data
            // $log.info(data);
            //console.log('error');
          });

      //------Dynamic End------------------------

    })

    .controller('PuSlugpageCtrl', function PuSlugpageCtrl($scope, $http, $stateParams, $rootScope) {
        // This is simple a demo for UI Boostrap.

        //console.log('stateparm', $stateParams);

        //------Dynamic Start----------------------

        $scope.pageslug = $stateParams.slug;
         $scope.content={};

        $http.get($rootScope.apipath+"/page?slug=" + $scope.pageslug)
            .success(function (data) {
                //On successful API CALL check whether empty data is returned or not

                //console.log('slug data', data);
                $scope.content = data;
            })
            .error(function (data) {
                //Log error Data
                // $log.info(data);
                //console.log('error');
            });

        //------Dynamic End------------------------

    });
