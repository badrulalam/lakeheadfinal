angular.module('ngBoilerplate.playgrounds', [
    'ui.router',
    'placeholders',
    'ui.bootstrap'
])

    .config(function config($stateProvider) {
        $stateProvider
            .state('playgrounds', {
                url: '/playgrounds',
                views: {
                    "main": {
                        controller: 'PlaygroundsCtrl',
                        templateUrl: 'playgrounds.tpl.html'
                    }
                },
                data: {pageTitle: 'playgrounds'}
            });
    })

    .controller('PlaygroundsCtrl', function PlaygroundsCtrl($scope, $http, $rootScope) {
        // This is simple a demo for UI Boostrap.
        $scope.dropdownDemoItems = [
            "The first choice!",
            "And another choice for you.",
            "but wait! A third!"
        ];
    });
