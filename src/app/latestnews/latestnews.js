angular.module('ngBoilerplate.latestnews', [
    'ui.router',
    'placeholders',
    'ui.bootstrap'
])

    .config(function config($stateProvider) {
        $stateProvider
            .state('latestnews', {
                url: '/latestnews',
                views: {
                    "main": {
                        controller: 'LatestnewsCtrl',
                        templateUrl: 'latestnews.tpl.html'
                    }
                },
                data: {pageTitle: 'latestnews'}
            });
    })

    .controller('LatestnewsCtrl', function LatestnewsCtrl($scope, $http, $rootScope) {

    });
