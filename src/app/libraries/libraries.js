angular.module('ngBoilerplate.libraries', [
    'ui.router',
    'placeholders',
    'ui.bootstrap'
])

    .config(function config($stateProvider) {
        $stateProvider
            .state('libraries', {
                url: '/libraries',
                views: {
                    "main": {
                        controller: 'LibrariesCtrl',
                        templateUrl: 'libraries.tpl.html'
                    }
                },
                data: {pageTitle: 'libraries'}
            });
    })

    .controller('LibrariesCtrl', function LibrariesCtrl($scope, $http, $rootScope) {

    });
