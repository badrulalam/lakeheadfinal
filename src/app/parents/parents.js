angular.module('ngBoilerplate.parents', [
    'ui.router',
    'placeholders',
    'ui.bootstrap'
])

    .config(function config($stateProvider) {
        $stateProvider.state('parents', {
            url: '/parents',
            views: {
                "main": {
                    controller: 'ParentsCtrl',
                    templateUrl: 'parents/parents.tpl.html'
                }
            },
            data: {pageTitle: 'parents'}
        })
            .state('parents.slugpage', {
                url: '/:slug',
                views: {
                    "pupils": {
                        controller: 'PrSlugpageCtrl',
                        templateUrl: 'parents/slugpage.tpl.html'
                    }
                }
            });
    })

    .controller('ParentsCtrl', function ParentsCtrl($scope, $http, $rootScope) {
        // This is simple a demo for UI Boostrap.
        $scope.dropdownDemoItems = [
            "The first choice!",
            "And another choice for you.",
            "but wait! A third!"
        ];


        //------Dynamic Start----------------------

        $scope.pageslug = 'parents';
        // $scope.content='asdds';

        $http.get($rootScope.apipath + "/page?slug=" + $scope.pageslug)
            .success(function (data) {
                //On successful API CALL check whether empty data is returned or not

                //console.log('slug data', data);
                $scope.content = data;
            })
            .error(function (data) {
                //Log error Data
                // $log.info(data);
                //console.log('error');
            });


        //------Dynamic End------------------------


    })

    .controller('PrSlugpageCtrl', function PrSlugpageCtrl($scope, $http, $stateParams, $rootScope) {
        // This is simple a demo for UI Boostrap.

        //console.log('stateparm', $stateParams);

        //------Dynamic Start----------------------

        $scope.pageslug = $stateParams.slug;
        $scope.content = {};

        $http.get($rootScope.apipath + "/page?slug=" + $scope.pageslug)
            .success(function (data) {
                //On successful API CALL check whether empty data is returned or not

                //console.log('slug data', data);
                $scope.content = data;
            })
            .error(function (data) {
                //Log error Data
                // $log.info(data);
                //console.log('error');
            });


        //------Dynamic End------------------------


    });
